using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnchantmentController : MonoBehaviour
{

    private static EnchantmentController instance;

    private void Awake()
    {
        if (instance != null)
        {
            if (instance != this)
            {
                Destroy(this);
            }
        }
        else
        {
            instance = this;
        }
    }
    internal static EnchantmentController GetInstance()
    {
        return instance;
    }

    internal BestMoveEnchantment NewBestMoveEnchantment()
    {
        GameObject enchObj = new GameObject("BestMoveEnchantment");

        BestMoveEnchantment enchantment = enchObj.AddComponent<BestMoveEnchantment>();

        return enchantment;
    }

}
