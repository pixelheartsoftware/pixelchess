using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BestMoveEnchantment : Enchantment<PieceController>
{
    private static float shakeAmount = 0.01F;

    private Vector2 basePos;

    private SpriteRenderer targetSpriteRenderer;

    private bool colorChangeWay;
    private Color color1 = Color.white;
    private Color color2 = Color.yellow;
    private float colorChangeSpeed = 0.05F;

    protected override void OnEnchant()
    {
        basePos = target.transform.position;
        targetSpriteRenderer = target.GetComponent<SpriteRenderer>();
    }

    void Update()
    {
        if (target == null)
        {
            return;
        }

        if (colorChangeWay)
        {
            targetSpriteRenderer.color = Color.Lerp(targetSpriteRenderer.color, color1, colorChangeSpeed);
            if (targetSpriteRenderer.color == color1)
            {
                colorChangeWay = !colorChangeWay;
            }
        } else
        {
            targetSpriteRenderer.color = Color.Lerp(targetSpriteRenderer.color, color2, colorChangeSpeed);
            if (targetSpriteRenderer.color == color2)
            {
                colorChangeWay = !colorChangeWay;
            }
        }


        Move bestMove = ChessboardManager.GetInstance().GetPonder();
        if ((bestMove != null) && (bestMove.from == target.item.coord))
        {
            target.transform.position += new Vector3(Random.Range(-shakeAmount, shakeAmount), Random.Range(-shakeAmount, shakeAmount), 0F);
        }
    }
}
