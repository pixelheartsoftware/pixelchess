using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HintController : MonoBehaviour
{
    public string text;

    private void OnMouseOver()
    {
        Hint.GetInstance().Show(text);
    }

    private void OnMouseExit()
    {
        Hint.GetInstance().Hide();
    }

}
