using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScrollController : MonoBehaviour
{

    private void OnMouseUpAsButton()
    {
        Move ponder = ChessboardManager.GetInstance().GetPonder();

        if (ponder == null)
        {
            return;
        }
        ChessboardManager.GetInstance().HighlightMove(ponder);
        Destroy(gameObject);
    }

}
