using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Enchantment<T> : MonoBehaviour where T : MonoBehaviour
{
    protected Enchantable<T> target;
    public void Enchant(Enchantable<T> target)
    {
        this.target = target;
        this.target.Enchant(this);

        OnEnchant();
    }

    protected abstract void OnEnchant();

}

public abstract class Enchantable<T> : MonoBehaviour where T : MonoBehaviour
{
    public List<Enchantment<T>> enchantments = new List<Enchantment<T>>();

    public T item;

    internal void Enchant(Enchantment<T> enchantment)
    {
        enchantments.Add(enchantment);
        item = GetComponent<T>();
    }
}