using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DraggingController : MonoBehaviour
{
    private PieceController piece;

    private HashSet<SquareController> hoveringOver = new HashSet<SquareController>();

    void Start()
    {
        piece = GetComponent<PieceController>();
    }

    private void OnMouseDown()
    {
        ChessboardManager.GetInstance().HighlightLegalMoves(piece);
    }

    void OnMouseDrag()
    {
        piece.dragged = true;
        transform.position = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        transform.position = new Vector2(transform.position.x, transform.position.y);
    }

    void OnMouseUp()
    {
        piece.dragged = false;

        ChessboardManager.GetInstance().UnhighlightLegalMoves();

        SquareController nearest = FindNearestSquare();
        if (nearest == null)
        {
            return;
        }

        ChessboardManager.GetInstance().playerMove = new Move(piece.coord, nearest.coord);
    }

    private SquareController FindNearestSquare()
    {
        float min = float.MaxValue;
        SquareController nearest = null;

        foreach (SquareController sq in hoveringOver)
        {
            float dist = Vector2.Distance(transform.position, sq.transform.position);
            if (dist < min)
            {
                min = dist;
                nearest = sq;
            }
        }
        return nearest;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Square"))
        {
            hoveringOver.Add(collision.GetComponent<SquareController>());
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("Square"))
        {
            hoveringOver.Remove(collision.GetComponent<SquareController>());
        }
    }

}
