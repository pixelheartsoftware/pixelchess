﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PiecesManager : MonoBehaviour
{
    private Dictionary<PieceType, Sprite> whiteSprites = new Dictionary<PieceType, Sprite>();
    private Dictionary<PieceType, Sprite> blackSprites = new Dictionary<PieceType, Sprite>();

    private Dictionary<PieceType, GameObject> prefabs = new Dictionary<PieceType, GameObject>();

    public static bool WHITE = true;
    public static bool BLACK = false;

    public PieceController CreatePiece(PieceType type, PieceColor color)
    {
        GameObject prefab = GetPrefab(type);

        Sprite sprite = GetSprite(type, color);

        GameObject piece = Instantiate(prefab);

        piece.GetComponent<SpriteRenderer>().sprite = sprite;

        PieceController pieceComp = piece.GetComponent<PieceController>();
        pieceComp.piece = new Piece(type, color);

        return pieceComp;
    }

    private GameObject GetPrefab(PieceType type)
    {
        return prefabs[type];
    }

    private Sprite GetSprite(PieceType type, PieceColor color)
    {
        return (color == PieceColor.White ? whiteSprites : blackSprites)[type];
    }

    // nevermind that
    public PieceTypeToSprite[] _whiteSprites;
    public PieceTypeToSprite[] _blackSprites;

    public PieceTypeToPrefab[] _prefabs;

    private void Start()
    {
        if (instance != null && instance != this)
        {
            GameObject.Destroy(instance);
        }
        instance = this;

        foreach (PieceTypeToSprite mapping in _whiteSprites)
        {
            whiteSprites.Add(mapping.type, mapping.sprite);
        }
        foreach (PieceTypeToSprite mapping in _blackSprites)
        {
            blackSprites.Add(mapping.type, mapping.sprite);
        }
        foreach (PieceTypeToPrefab mapping in _prefabs)
        {
            prefabs.Add(mapping.type, mapping.prefab);
        }
    }

    private static PiecesManager instance;

    public static PiecesManager GetInstance()
    {
        return instance;
    }
}

[Serializable]
public struct PieceTypeToSprite
{
    public PieceType type;
    public Sprite sprite;
}

[Serializable]
public struct PieceTypeToPrefab
{
    public PieceType type;
    public GameObject prefab;
}