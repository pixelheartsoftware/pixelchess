using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PotionController : MonoBehaviour
{

    public float timeMinutes;

    public void OnMouseUpAsButton()
    {
        ChessClockController.GetInstance().IncreasePlayerTime(timeMinutes);
        Destroy(gameObject);
    }

}
