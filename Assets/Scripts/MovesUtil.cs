using System;
using System.Collections.Generic;
using UnityEngine;

public class MovesUtil
{

    private static int[,] KNIGHT_ATTACKS = new int[,] {
        {-2, -1},
        {-2, 1},
        {-1, -2},
        {-1, 2},
        {2, -1},
        {2, 1},
        {1, -2},
        {1, 2}
    };

    internal static HashSet<Move> GetMoves(Coordinate from, GameDescription gameDescription)
    {
        Piece movingPiece = gameDescription.GetPiece(from);

        BoardInfo boardInfo = GetBoardInfo(gameDescription);

        HashSet<Move> moves = new HashSet<Move>();

        foreach (KeyValuePair<Coordinate, SquareInfo> entry in boardInfo.moveMapping)
        {
            if (entry.Value.GetAllDirectMovers().Contains(from))
            {
                if (!gameDescription.IsOccupied(entry.Key))
                {
                    moves.Add(new Move(from, entry.Key));
                }
            }
            if (entry.Value.GetAllDirectAttackers().Contains(from))
            {
                if (gameDescription.IsOccupied(entry.Key, movingPiece.color.Opposite()))
                {
                    moves.Add(new Move(from, entry.Key));
                }
            }
        }

        // Potential castling:
        if (movingPiece.type == PieceType.King && !IsAttacked(from, movingPiece.color.Opposite(), boardInfo, gameDescription))
        {
            if (gameDescription.CanCastle(PieceColor.White, CastlingType.OO))
            {
                if (!IsAttacked(Coordinate.F1, movingPiece.color.Opposite(), boardInfo, gameDescription) &&
                    !IsAttacked(Coordinate.G1, movingPiece.color.Opposite(), boardInfo, gameDescription) &&
                    !gameDescription.IsOccupied(Coordinate.F1) &&
                    !gameDescription.IsOccupied(Coordinate.G1))
                {
                    moves.Add(new Move(from, Coordinate.G1));
                }
            }
            if (gameDescription.CanCastle(PieceColor.White, CastlingType.OOO))
            {
                if (!IsAttacked(Coordinate.D1, movingPiece.color.Opposite(), boardInfo, gameDescription) &&
                    !IsAttacked(Coordinate.C1, movingPiece.color.Opposite(), boardInfo, gameDescription) &&
                    !gameDescription.IsOccupied(Coordinate.D1) &&
                    !gameDescription.IsOccupied(Coordinate.C1))
                {
                    moves.Add(new Move(from, Coordinate.C1));
                }
            }
            if (gameDescription.CanCastle(PieceColor.Black, CastlingType.OO))
            {
                if (!IsAttacked(Coordinate.F8, movingPiece.color.Opposite(), boardInfo, gameDescription) &&
                    !IsAttacked(Coordinate.G8, movingPiece.color.Opposite(), boardInfo, gameDescription) &&
                    !gameDescription.IsOccupied(Coordinate.F8) &&
                    !gameDescription.IsOccupied(Coordinate.G8))
                {
                    moves.Add(new Move(from, Coordinate.G8));
                }
            }
            if (gameDescription.CanCastle(PieceColor.Black, CastlingType.OOO))
            {
                if (!IsAttacked(Coordinate.D8, movingPiece.color.Opposite(), boardInfo, gameDescription) &&
                    !IsAttacked(Coordinate.C8, movingPiece.color.Opposite(), boardInfo, gameDescription) &&
                    !gameDescription.IsOccupied(Coordinate.D8) &&
                    !gameDescription.IsOccupied(Coordinate.C8))
                {
                    moves.Add(new Move(from, Coordinate.C8));
                }
            }
        }

        // en passant:
        if (movingPiece.type == PieceType.Pawn)
        {
            Move lastMove = gameDescription.GetLastMove();
            if (lastMove != null)
            {
                int fileDifference = lastMove.to.file - from.file;

                if (movingPiece.color == PieceColor.White &&
                    from.rank == Coordinate.Rank.R5 &&
                    lastMove.from.rank == Coordinate.Rank.R7 &&
                    lastMove.to.rank == Coordinate.Rank.R5 &&
                    Math.Abs(fileDifference) == 1)
                {
                    Coordinate.File newFile = (from.file + fileDifference);
                    Coordinate.Rank newRank = Coordinate.Rank.R6;
                    moves.Add(new Move(from, Coordinate.Get(newFile, newRank)));
                }
                else if (movingPiece.color == PieceColor.Black &&
                  from.rank == Coordinate.Rank.R4 &&
                  lastMove.from.rank == Coordinate.Rank.R2 &&
                  lastMove.to.rank == Coordinate.Rank.R4 &&
                  Math.Abs(fileDifference) == 1)
                {
                    Coordinate.File newFile = (from.file + fileDifference);
                    Coordinate.Rank newRank = Coordinate.Rank.R3;
                    moves.Add(new Move(from, Coordinate.Get(newFile, newRank)));
                }
            }
        }

        moves.RemoveWhere((move) => {
            return MoveCausesCheckedState(move, gameDescription);
        });

        return moves;
    }

    internal static bool IsPromotion(Move move, GameDescription gameDescription)
    {
        return gameDescription.GetPiece(move.from).type == PieceType.Pawn && (move.to.rank == Coordinate.Rank.R1 || move.to.rank == Coordinate.Rank.R8);
    }

    internal static Coordinate GetEnPassantTakeCoord(Move move)
    {
        Coordinate.File takeFile = move.to.file;
        Coordinate.Rank takeRank = move.from.rank;

        return Coordinate.Get(takeFile, takeRank);
    }

    internal static bool IsEnPassant(Move move, GameDescription gameDescription)
    {
        return gameDescription.GetPiece(move.from).type == PieceType.Pawn &&
            Piece.NULL.Equals(gameDescription.GetPiece(move.to)) &&
            move.from.file != move.to.file;
    }

    private static bool IsAttacked(Coordinate from, PieceColor pieceColor, BoardInfo boardInfo, GameDescription game)
    {
        foreach(Coordinate attacker in boardInfo.moveMapping[from].GetAllDirectAttackers())
        {
            if (game.GetPiece(attacker).color == pieceColor)
            {
                return true;
            }
        }
        return false;
    }

    private static bool MoveCausesCheckedState(Move move, GameDescription boardDescOriginal)
    {
        GameDescription boardAfterMove = boardDescOriginal.Copy();
        boardAfterMove.Move(move);

        BoardInfo boardInfoAfterMove = GetBoardInfo(boardAfterMove);

        PieceColor movingPieceColor = boardDescOriginal.GetPiece(move.from).color;

        foreach (KeyValuePair<Coordinate, Piece> squares in boardAfterMove.Squares())
        {
            if (!Piece.NULL.Equals(squares.Value) &&
                squares.Value.type == PieceType.King &&
                squares.Value.color == movingPieceColor)
            {
                List<Coordinate> kingAttackers = boardInfoAfterMove.moveMapping[squares.Key].GetAllDirectAttackers();

                foreach (Coordinate attackerCoord in kingAttackers)
                {
                    if (boardAfterMove.IsOccupied(attackerCoord, movingPieceColor.Opposite()))
                    {
                        return true;
                    }
                }
            }
        }

        return false;
    }

    private static BoardInfo GetBoardInfo(GameDescription pieces)
    {
        HashSet<PieceType> diagonalTypes = new HashSet<PieceType>();
        diagonalTypes.Add(PieceType.Queen);
        diagonalTypes.Add(PieceType.Bishop);

        HashSet<PieceType> rankFileTypes = new HashSet<PieceType>();
        rankFileTypes.Add(PieceType.Queen);
        rankFileTypes.Add(PieceType.Rook);

        BoardInfo boardInfo = new BoardInfo();

        boardInfo.moveMapping = new Dictionary<Coordinate, SquareInfo>();

        foreach (KeyValuePair<Coordinate, Piece> entry in pieces.Squares())
        {
            Coordinate orig = entry.Key;

            SquareInfo sqInfo = new SquareInfo();


            sqInfo.piecesUpRight = GetLineAttackers(orig, 1, 1, diagonalTypes, pieces);
            sqInfo.piecesUpLeft = GetLineAttackers(orig, -1, 1, diagonalTypes, pieces);
            sqInfo.piecesDownLeft = GetLineAttackers(orig, -1, -1, diagonalTypes, pieces);
            sqInfo.piecesDownRight = GetLineAttackers(orig, 1, -1, diagonalTypes, pieces);

            sqInfo.piecesUp = GetLineAttackers(orig, 0, 1, rankFileTypes, pieces);
            sqInfo.piecesLeft = GetLineAttackers(orig, -1, 0, rankFileTypes, pieces);
            sqInfo.piecesDown = GetLineAttackers(orig, 0, -1, rankFileTypes, pieces);
            sqInfo.piecesRight = GetLineAttackers(orig, 1, 0, rankFileTypes, pieces);

            sqInfo.knights = GetKnightAttackers(orig, pieces);

            boardInfo.moveMapping.Add(orig, sqInfo);
        }
        return boardInfo;
    }

    private static List<PieceMoveInfo> GetKnightAttackers(Coordinate orig, GameDescription board)
    {
        List<PieceMoveInfo> knightMoves = new List<PieceMoveInfo>();

        for (int i = 0; i < KNIGHT_ATTACKS.GetLength(0); i++)
        {
            Coordinate newCoord = GetCoordIfExists((int)orig.file + KNIGHT_ATTACKS[i, 0], (int)orig.rank + KNIGHT_ATTACKS[i, 1]);
            if (newCoord == null)
            {
                continue;
            }

            Piece piece = board.GetPiece(newCoord);
            if (Piece.NULL.Equals(piece))
            {
                continue;
            }

            if (piece.type == PieceType.Knight)
            {
                PieceMoveInfo info = new PieceMoveInfo();

                info.coord = newCoord;
                info.attacks = true;
                info.canMove = true;
                info.piece = new Piece(piece.type, piece.color);

                knightMoves.Add(info);
            }
        }
        return knightMoves;
    }

    private static List<PieceMoveInfo> GetLineAttackers(Coordinate orig, int fPlus, int rPlus, HashSet<PieceType> attackers, GameDescription boardDescription)
    {
        List<PieceMoveInfo> result = new List<PieceMoveInfo>();



        Coordinate newCoord = orig;
        while ((newCoord = GetCoordIfExists((int)newCoord.file + fPlus, (int)newCoord.rank + rPlus)) != null)
        {
            Piece piece = boardDescription.GetPiece(newCoord);
            if (!Piece.NULL.Equals(piece))
            {
                PieceMoveInfo info = new PieceMoveInfo();

                info.coord = newCoord;
                info.piece = new Piece(piece.type, piece.color);
                info.attacks = attackers.Contains(piece.type);
                info.canMove = info.attacks;

                if (!info.attacks && piece.type == PieceType.Pawn) // checking for pawn attack
                {
                    int fileDiff = newCoord.file - orig.file;
                    int rankDiff = newCoord.rank - orig.rank;

                    if ((fileDiff == 1 || fileDiff == -1) && ((piece.color == PieceColor.White && rankDiff == -1) || (piece.color == PieceColor.Black && rankDiff == 1)))
                    {
                        info.attacks = true;
                        info.canMove = false;
                    }

                    if (fileDiff == 0)
                    {
                        if (piece.color == PieceColor.White)
                        {
                            if (newCoord.rank == Coordinate.Rank.R2 && orig.rank == Coordinate.Rank.R4)
                            {
                                info.canMove = true;
                            } else if (rankDiff == -1)
                            {
                                info.canMove = true;
                            }
                        } else
                        {
                            if (newCoord.rank == Coordinate.Rank.R7 && orig.rank == Coordinate.Rank.R5)
                            {
                                info.canMove = true;
                            }
                            else if (rankDiff == 1)
                            {
                                info.canMove = true;
                            }
                        }
                    }
                }

                if (piece.type == PieceType.King)
                {
                    // checking distance, should be 1 square away:
                    int distSquared = (int)(Mathf.Pow(orig.file - newCoord.file, 2) + Mathf.Pow(orig.rank - newCoord.rank, 2));

                    if (distSquared == 2 || distSquared == 1)
                    {
                        info.attacks = true;
                        info.canMove = true;
                    }
                }

                result.Add(info);
            }
        }

        return result;
    }


    public static Coordinate GetCoordIfExists(int file, int rank)
    {
        if (file < 0 || file > 7 || rank < 0 || rank > 7)
        {
            return null;
        }

        return Coordinate.Get((Coordinate.File)file, (Coordinate.Rank)rank);
    }

}

public struct BoardInfo
{
    public Dictionary<Coordinate, SquareInfo> moveMapping;

}

public struct SquareInfo
{
    public List<PieceMoveInfo> piecesUpRight;
    public List<PieceMoveInfo> piecesUpLeft;
    public List<PieceMoveInfo> piecesDownLeft;
    public List<PieceMoveInfo> piecesDownRight;

    public List<PieceMoveInfo> piecesRight;
    public List<PieceMoveInfo> piecesLeft;
    public List<PieceMoveInfo> piecesUp;
    public List<PieceMoveInfo> piecesDown;

    public List<PieceMoveInfo> knights;

    internal SquareInfo Copy()
    {
        SquareInfo copy = new SquareInfo();

        copy.piecesUpRight = new List<PieceMoveInfo>(piecesUpRight);
        copy.piecesUpLeft = new List<PieceMoveInfo>(piecesUpLeft);
        copy.piecesDownLeft = new List<PieceMoveInfo>(piecesDownLeft);
        copy.piecesDownRight = new List<PieceMoveInfo>(piecesDownRight);
        copy.piecesRight = new List<PieceMoveInfo>(piecesRight);
        copy.piecesLeft = new List<PieceMoveInfo>(piecesLeft);
        copy.piecesUp = new List<PieceMoveInfo>(piecesUp);
        copy.piecesDown = new List<PieceMoveInfo>(piecesDown);
        copy.knights = new List<PieceMoveInfo>(knights);

        return copy;
    }

    public List<Coordinate> GetAllDirectAttackers()
    {
        List<Coordinate> allDirectAttackers = new List<Coordinate>();

        if (piecesUpRight.Count > 0 && piecesUpRight[0].attacks)
            allDirectAttackers.Add(piecesUpRight[0].coord);
        if (piecesUpLeft.Count > 0 && piecesUpLeft[0].attacks)
            allDirectAttackers.Add(piecesUpLeft[0].coord);
        if (piecesDownLeft.Count > 0 && piecesDownLeft[0].attacks)
            allDirectAttackers.Add(piecesDownLeft[0].coord);
        if (piecesDownRight.Count > 0 && piecesDownRight[0].attacks)
            allDirectAttackers.Add(piecesDownRight[0].coord);

        if (piecesRight.Count > 0 && piecesRight[0].attacks)
            allDirectAttackers.Add(piecesRight[0].coord);
        if (piecesLeft.Count > 0 && piecesLeft[0].attacks)
            allDirectAttackers.Add(piecesLeft[0].coord);
        if (piecesUp.Count > 0 && piecesUp[0].attacks)
            allDirectAttackers.Add(piecesUp[0].coord);
        if (piecesDown.Count > 0 && piecesDown[0].attacks)
            allDirectAttackers.Add(piecesDown[0].coord);

        foreach (PieceMoveInfo knightInfo in knights)
        {
            allDirectAttackers.Add(knightInfo.coord);
        }

        return allDirectAttackers;
    }

    public List<Coordinate> GetAllDirectMovers()
    {
        List<Coordinate> allDirectMovers = new List<Coordinate>();

        if (piecesUpRight.Count > 0 && piecesUpRight[0].canMove)
            allDirectMovers.Add(piecesUpRight[0].coord);
        if (piecesUpLeft.Count > 0 && piecesUpLeft[0].canMove)
            allDirectMovers.Add(piecesUpLeft[0].coord);
        if (piecesDownLeft.Count > 0 && piecesDownLeft[0].canMove)
            allDirectMovers.Add(piecesDownLeft[0].coord);
        if (piecesDownRight.Count > 0 && piecesDownRight[0].canMove)
            allDirectMovers.Add(piecesDownRight[0].coord);

        if (piecesRight.Count > 0 && piecesRight[0].canMove)
            allDirectMovers.Add(piecesRight[0].coord);
        if (piecesLeft.Count > 0 && piecesLeft[0].canMove)
            allDirectMovers.Add(piecesLeft[0].coord);
        if (piecesUp.Count > 0 && piecesUp[0].canMove)
            allDirectMovers.Add(piecesUp[0].coord);
        if (piecesDown.Count > 0 && piecesDown[0].canMove)
            allDirectMovers.Add(piecesDown[0].coord);

        foreach (PieceMoveInfo knightInfo in knights)
        {
            allDirectMovers.Add(knightInfo.coord);
        }

        return allDirectMovers;
    }
}

public struct PieceMoveInfo
{
    public Piece piece;
    public bool attacks;
    public bool canMove;
    public Coordinate coord;
}