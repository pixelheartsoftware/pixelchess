﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using UnityEngine;

public class Coordinate
{
    public File file;
    public Rank rank;

    public Coordinate(File col, Rank rank)
    {
        this.file = col;
        this.rank = rank;
    }

    public enum File
    {
        A=0, 
        B=1, 
        C=2, 
        D=3, 
        E=4, 
        F=5, 
        G=6, 
        H=7
    }

    public enum Rank
    {
        [Description("1")]
        R1 = 0,
        [Description("2")]
        R2 = 1,
        [Description("3")]
        R3 = 2,
        [Description("4")]
        R4 = 3,
        [Description("5")]
        R5 = 4,
        [Description("6")]
        R6 = 5,
        [Description("7")]
        R7 = 6,
        [Description("8")]
        R8 = 7
    }

    internal string toUci()
    {
        return file.ToString().ToLower() + ((int)rank + 1);
    }

    internal static Coordinate fromUci(string value)
    {
        string f = value[0].ToString();
        string r = "R" + value[1].ToString();

        File file = (File) Enum.Parse(typeof(File), f, true);
        Rank rank = (Rank) Enum.Parse(typeof(Rank), r, true);

        return Coordinate.Get(file, rank);
    }


    public override int GetHashCode()
    {
        int hash = 17;
        hash = hash * 23 + file.GetHashCode();
        hash = hash * 23 + rank.GetHashCode();
        return hash;
    }

    public override bool Equals(object obj)
    {
        Coordinate other = (Coordinate) obj;
        return file == other.file && rank == other.rank;
    }

    public override string ToString()
    {
        return file.ToString() + (((int)rank + 1));
    }

    internal static Coordinate Get(File file, Rank rank)
    {
        return mapping[file][rank];
    }

    // for easy management:
    public static Coordinate A1 = new Coordinate(File.A, Rank.R1);
    public static Coordinate A2 = new Coordinate(File.A, Rank.R2);
    public static Coordinate A3 = new Coordinate(File.A, Rank.R3);
    public static Coordinate A4 = new Coordinate(File.A, Rank.R4);
    public static Coordinate A5 = new Coordinate(File.A, Rank.R5);
    public static Coordinate A6 = new Coordinate(File.A, Rank.R6);
    public static Coordinate A7 = new Coordinate(File.A, Rank.R7);
    public static Coordinate A8 = new Coordinate(File.A, Rank.R8);

    public static Coordinate B1 = new Coordinate(File.B, Rank.R1);
    public static Coordinate B2 = new Coordinate(File.B, Rank.R2);
    public static Coordinate B3 = new Coordinate(File.B, Rank.R3);
    public static Coordinate B4 = new Coordinate(File.B, Rank.R4);
    public static Coordinate B5 = new Coordinate(File.B, Rank.R5);
    public static Coordinate B6 = new Coordinate(File.B, Rank.R6);
    public static Coordinate B7 = new Coordinate(File.B, Rank.R7);
    public static Coordinate B8 = new Coordinate(File.B, Rank.R8);

    public static Coordinate C1 = new Coordinate(File.C, Rank.R1);
    public static Coordinate C2 = new Coordinate(File.C, Rank.R2);
    public static Coordinate C3 = new Coordinate(File.C, Rank.R3);
    public static Coordinate C4 = new Coordinate(File.C, Rank.R4);
    public static Coordinate C5 = new Coordinate(File.C, Rank.R5);
    public static Coordinate C6 = new Coordinate(File.C, Rank.R6);
    public static Coordinate C7 = new Coordinate(File.C, Rank.R7);
    public static Coordinate C8 = new Coordinate(File.C, Rank.R8);

    public static Coordinate D1 = new Coordinate(File.D, Rank.R1);
    public static Coordinate D2 = new Coordinate(File.D, Rank.R2);
    public static Coordinate D3 = new Coordinate(File.D, Rank.R3);
    public static Coordinate D4 = new Coordinate(File.D, Rank.R4);
    public static Coordinate D5 = new Coordinate(File.D, Rank.R5);
    public static Coordinate D6 = new Coordinate(File.D, Rank.R6);
    public static Coordinate D7 = new Coordinate(File.D, Rank.R7);
    public static Coordinate D8 = new Coordinate(File.D, Rank.R8);

    public static Coordinate E1 = new Coordinate(File.E, Rank.R1);
    public static Coordinate E2 = new Coordinate(File.E, Rank.R2);
    public static Coordinate E3 = new Coordinate(File.E, Rank.R3);
    public static Coordinate E4 = new Coordinate(File.E, Rank.R4);
    public static Coordinate E5 = new Coordinate(File.E, Rank.R5);
    public static Coordinate E6 = new Coordinate(File.E, Rank.R6);
    public static Coordinate E7 = new Coordinate(File.E, Rank.R7);
    public static Coordinate E8 = new Coordinate(File.E, Rank.R8);

    public static Coordinate F1 = new Coordinate(File.F, Rank.R1);
    public static Coordinate F2 = new Coordinate(File.F, Rank.R2);
    public static Coordinate F3 = new Coordinate(File.F, Rank.R3);
    public static Coordinate F4 = new Coordinate(File.F, Rank.R4);
    public static Coordinate F5 = new Coordinate(File.F, Rank.R5);
    public static Coordinate F6 = new Coordinate(File.F, Rank.R6);
    public static Coordinate F7 = new Coordinate(File.F, Rank.R7);
    public static Coordinate F8 = new Coordinate(File.F, Rank.R8);

    public static Coordinate G1 = new Coordinate(File.G, Rank.R1);
    public static Coordinate G2 = new Coordinate(File.G, Rank.R2);
    public static Coordinate G3 = new Coordinate(File.G, Rank.R3);
    public static Coordinate G4 = new Coordinate(File.G, Rank.R4);
    public static Coordinate G5 = new Coordinate(File.G, Rank.R5);
    public static Coordinate G6 = new Coordinate(File.G, Rank.R6);
    public static Coordinate G7 = new Coordinate(File.G, Rank.R7);
    public static Coordinate G8 = new Coordinate(File.G, Rank.R8);

    public static Coordinate H1 = new Coordinate(File.H, Rank.R1);
    public static Coordinate H2 = new Coordinate(File.H, Rank.R2);
    public static Coordinate H3 = new Coordinate(File.H, Rank.R3);
    public static Coordinate H4 = new Coordinate(File.H, Rank.R4);
    public static Coordinate H5 = new Coordinate(File.H, Rank.R5);
    public static Coordinate H6 = new Coordinate(File.H, Rank.R6);
    public static Coordinate H7 = new Coordinate(File.H, Rank.R7);
    public static Coordinate H8 = new Coordinate(File.H, Rank.R8);

    private static Dictionary<File, Dictionary<Rank, Coordinate>> mapping = new Dictionary<File, Dictionary<Rank, Coordinate>>();

    static Coordinate()
    {
        Dictionary<Rank, Coordinate> lines = new Dictionary<Rank, Coordinate>();
        lines.Add(Rank.R1, A1);
        lines.Add(Rank.R2, A2);
        lines.Add(Rank.R3, A3);
        lines.Add(Rank.R4, A4);
        lines.Add(Rank.R5, A5);
        lines.Add(Rank.R6, A6);
        lines.Add(Rank.R7, A7);
        lines.Add(Rank.R8, A8);
        mapping.Add(File.A, lines);

        lines = new Dictionary<Rank, Coordinate>();
        lines.Add(Rank.R1, B1);
        lines.Add(Rank.R2, B2);
        lines.Add(Rank.R3, B3);
        lines.Add(Rank.R4, B4);
        lines.Add(Rank.R5, B5);
        lines.Add(Rank.R6, B6);
        lines.Add(Rank.R7, B7);
        lines.Add(Rank.R8, B8);
        mapping.Add(File.B, lines);

        lines = new Dictionary<Rank, Coordinate>();
        lines.Add(Rank.R1, C1);
        lines.Add(Rank.R2, C2);
        lines.Add(Rank.R3, C3);
        lines.Add(Rank.R4, C4);
        lines.Add(Rank.R5, C5);
        lines.Add(Rank.R6, C6);
        lines.Add(Rank.R7, C7);
        lines.Add(Rank.R8, C8);
        mapping.Add(File.C, lines);

        lines = new Dictionary<Rank, Coordinate>();
        lines.Add(Rank.R1, D1);
        lines.Add(Rank.R2, D2);
        lines.Add(Rank.R3, D3);
        lines.Add(Rank.R4, D4);
        lines.Add(Rank.R5, D5);
        lines.Add(Rank.R6, D6);
        lines.Add(Rank.R7, D7);
        lines.Add(Rank.R8, D8);
        mapping.Add(File.D, lines);

        lines = new Dictionary<Rank, Coordinate>();
        lines.Add(Rank.R1, E1);
        lines.Add(Rank.R2, E2);
        lines.Add(Rank.R3, E3);
        lines.Add(Rank.R4, E4);
        lines.Add(Rank.R5, E5);
        lines.Add(Rank.R6, E6);
        lines.Add(Rank.R7, E7);
        lines.Add(Rank.R8, E8);
        mapping.Add(File.E, lines);

        lines = new Dictionary<Rank, Coordinate>();
        lines.Add(Rank.R1, F1);
        lines.Add(Rank.R2, F2);
        lines.Add(Rank.R3, F3);
        lines.Add(Rank.R4, F4);
        lines.Add(Rank.R5, F5);
        lines.Add(Rank.R6, F6);
        lines.Add(Rank.R7, F7);
        lines.Add(Rank.R8, F8);
        mapping.Add(File.F, lines);

        lines = new Dictionary<Rank, Coordinate>();
        lines.Add(Rank.R1, G1);
        lines.Add(Rank.R2, G2);
        lines.Add(Rank.R3, G3);
        lines.Add(Rank.R4, G4);
        lines.Add(Rank.R5, G5);
        lines.Add(Rank.R6, G6);
        lines.Add(Rank.R7, G7);
        lines.Add(Rank.R8, G8);
        mapping.Add(File.G, lines);

        lines = new Dictionary<Rank, Coordinate>();
        lines.Add(Rank.R1, H1);
        lines.Add(Rank.R2, H2);
        lines.Add(Rank.R3, H3);
        lines.Add(Rank.R4, H4);
        lines.Add(Rank.R5, H5);
        lines.Add(Rank.R6, H6);
        lines.Add(Rank.R7, H7);
        lines.Add(Rank.R8, H8);
        mapping.Add(File.H, lines);
    }

    internal bool OnSameFile(Coordinate coord)
    {
        return this.file == coord.file;
    }

    internal bool OnSameRank(Coordinate coord)
    {
        return this.rank == coord.rank;
    }

    internal bool OnSameDiagonal(Coordinate coord)
    {
        return ((int)file - rank == (int)coord.file - coord.rank) || ((int)file + rank == (int)coord.file + coord.rank);
    }
}
