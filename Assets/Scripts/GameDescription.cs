using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static Coordinate;

public class GameDescription
{
    private List<Move> moves = new List<Move>();

    private Dictionary<Coordinate, Piece> currentState = new Dictionary<Coordinate, Piece>();

    // the number of move that made castling impossible:
    private int whiteOORestrictedMove = -1;
    private int whiteOOORestrictedMove = -1;
    private int blackOORestrictedMove = -1;
    private int blackOOORestrictedMove = -1;

    public void Move(Move move)
    {
        bool isEnPassant = MovesUtil.IsEnPassant(move, this);
        bool isPromotion = MovesUtil.IsPromotion(move, this);

        Piece piece = currentState[move.from];
        currentState[move.from] = Piece.NULL;
        currentState[move.to] = piece;

        moves.Add(move);

        if (move.IsCastling() == CastlingType.OO)
        {
            if (piece.color == PieceColor.White)
            {
                Piece rook = currentState[Coordinate.H1];
                currentState[Coordinate.F1] = rook;
                currentState[Coordinate.H1] = Piece.NULL;
            }
            else
            {
                Piece rook = currentState[Coordinate.H8];
                currentState[Coordinate.F8] = rook;
                currentState[Coordinate.H8] = Piece.NULL;
            }
        }
        else if (move.IsCastling() == CastlingType.OOO)
        {
            if (piece.color == PieceColor.White)
            {
                Piece rook = currentState[Coordinate.A1];
                currentState[Coordinate.D1] = rook;
                currentState[Coordinate.A1] = Piece.NULL;
            }
            else
            {
                Piece rook = currentState[Coordinate.A8];
                currentState[Coordinate.D8] = rook;
                currentState[Coordinate.A8] = Piece.NULL;
            }
        }

        if (isEnPassant)
        {
            Coordinate takeCoord = MovesUtil.GetEnPassantTakeCoord(move);
            currentState[takeCoord] = Piece.NULL;
        }

        if (isPromotion)
        {
            Piece promoted = currentState[move.to];
            currentState[move.to] = new Piece(move.promotion, promoted.color);
        }

        // updating game metadata:
        if (piece.type == PieceType.King)
        {
            if (piece.color == PieceColor.White)
            {
                whiteOORestrictedMove = moves.Count;
                whiteOOORestrictedMove = moves.Count;
            } else
            {
                blackOORestrictedMove = moves.Count;
                blackOOORestrictedMove = moves.Count;
            }
        } else if (piece.type == PieceType.Rook)
        {
            if (piece.color == PieceColor.White)
            {
                if (whiteOORestrictedMove == -1 && move.from == H1)
                {
                    whiteOORestrictedMove = moves.Count;
                }
                if (whiteOOORestrictedMove == -1 && move.from == A1)
                {
                    whiteOOORestrictedMove = moves.Count;
                }
            }
            else
            {
                if (blackOORestrictedMove == -1 && move.from == H8)
                {
                    blackOORestrictedMove = moves.Count;
                }
                if (blackOOORestrictedMove == -1 && move.from == A8)
                {
                    blackOOORestrictedMove = moves.Count;
                }
            }
        }
    }

    public Move GetLastMove()
    {
        if (moves.Count == 0)
        {
            return null;
        }
        return moves[moves.Count - 1];
    }

    internal bool CanCastle(PieceColor color, CastlingType castlingType)
    {
        if (color == PieceColor.White)
        {
            if (castlingType == CastlingType.OO)
            {
                return whiteOORestrictedMove == -1;
            } else if (castlingType == CastlingType.OOO)
            {
                return whiteOOORestrictedMove == -1;
            }
        } else
        {
            if (castlingType == CastlingType.OO)
            {
                return blackOORestrictedMove == -1;
            }
            else if (castlingType == CastlingType.OOO)
            {
                return blackOOORestrictedMove == -1;
            }
        }
        throw new Exception("Cannot check castling possibility for: " + color + "/" + castlingType);
    }

    public Piece GetPiece(Coordinate coord)
    {
        return currentState[coord];
    }

    public IEnumerable<KeyValuePair<Coordinate, Piece>> Squares()
    {
        foreach(KeyValuePair<Coordinate, Piece> pair in currentState)
        {
            yield return pair;
        }
    }

    internal bool IsOccupied(Coordinate coord, PieceColor pieceColor)
    {
        Piece piece = GetPiece(coord);

        return (!Piece.NULL.Equals(piece)) && (piece.color == pieceColor);
    }

    internal bool IsOccupied(Coordinate coord)
    {
        return !Piece.NULL.Equals(GetPiece(coord));
    }

    internal GameDescription Copy()
    {
        GameDescription newDesc = new GameDescription();
        newDesc.moves = new List<Move>(moves);
        newDesc.currentState = new Dictionary<Coordinate, Piece>(currentState);
        return newDesc;
    }

    internal void Put(Coordinate coord, Piece piece)
    {
        currentState[coord] = piece;
    }

    public GameDescription()
    {
        foreach (File file in Enum.GetValues(typeof(File)))
        {
            foreach (Rank rank in Enum.GetValues(typeof(Rank)))
            {
                Coordinate coord = Coordinate.Get(file, rank);
                currentState[coord] = Piece.NULL;
            }
        }
    }

    internal string UciMoveSequence()
    {
        return string.Join<Move>(" ", moves);
    }
}

public class Move
{
    public Coordinate from;
    public Coordinate to;
    public PieceType promotion;

    public Move(Coordinate from, Coordinate to)
    {
        this.from = from;
        this.to = to;
        this.promotion = PieceType.Pawn;
    }

    public Move(Coordinate from, Coordinate to, PieceType promotion) : this(from, to)
    {
        this.promotion = promotion;
    }

    internal string ToUci()
    {
        return from.toUci() + to.toUci() + this.promotion.ToUci();
    }

    public override bool Equals(object ob)
    {
        if (ob is Move)
        {
            Move m = (Move)ob;
            return from.Equals(m.from) && to.Equals(m.to);
        }
        else
        {
            return false;
        }
    }

    public override int GetHashCode()
    {
        int hash = 17;
        hash = hash * 23 + from.GetHashCode();
        hash = hash * 23 + to.GetHashCode();
        return hash;
    }

    public override string ToString()
    {
        return ToUci();
    }

    public CastlingType IsCastling()
    {
        if ((from == E1 && to == G1) || (from == E8 && to == G8))
        {
            return CastlingType.OO;
        }
        if ((from == E1 && to == C1) || (from == E8 && to == C8))
        {
            return CastlingType.OOO;
        }
        return CastlingType.NONE;
    }
}


public struct Piece
{
    public readonly static Piece NULL = new Piece(PieceType.King, PieceColor.White, true);
    public readonly PieceType type;
    public readonly PieceColor color;
    private readonly bool isNull;

    public Piece(PieceType type, PieceColor color)
    {
        this.type = type;
        this.color = color;
        this.isNull = false;
    }

    private Piece(PieceType type, PieceColor color, bool isNull)
    {
        this.type = type;
        this.color = color;
        this.isNull = isNull;
    }

    public override bool Equals(object ob)
    {
        if (ob is Piece)
        {
            Piece p = (Piece)ob;
            return p.color == color && p.type == type && p.isNull == isNull;
        }
        else
        {
            return false;
        }
    }

    public override int GetHashCode()
    {
        int hash = 17;
        hash = hash * 23 + isNull.GetHashCode();
        hash = hash * 23 + type.GetHashCode();
        hash = hash * 23 + color.GetHashCode();
        return hash;
    }
}

public enum PieceType
{
    King, Queen, Bishop, Knight, Rook, Pawn
}

public enum PieceColor
{
    Black, White
}

public enum CastlingType
{
    NONE, OO, OOO
}