using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Hint : MonoBehaviour
{
    private Text text;

    void Start()
    {
        if (instance != null)
        {
            if (instance != this)
            {
                Destroy(this);
            }
        }
        else
        {
            instance = this;
        }

        text = GetComponent<Text>();
    }

    internal void Hide()
    {
        text.enabled = false;
    }

    internal void Show(string textString)
    {
        text.enabled = true;
        text.text = textString;
    }

    private static Hint instance;

    private void Update()
    {
        transform.position = Input.mousePosition;
    }

    internal static Hint GetInstance()
    {
        return instance;
    }


}
