using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TakenPiecesManager : MonoBehaviour
{
    public Transform whiteTray;
    public Transform blackTray;

    public GameObject tokenPrefab;

    List<Transform> takenBlack = new List<Transform>();
    List<Transform> takenWhite = new List<Transform>();

    public void Take(PieceController piece)
    {
        GameObject token = GameObject.Instantiate(tokenPrefab, (piece.color == PieceColor.White) ? whiteTray : blackTray);
        token.GetComponent<SpriteRenderer>().sprite = piece.GetComponent<SpriteRenderer>().sprite;

        if (piece.color == PieceColor.White)
        {
            takenWhite.Add(token.transform);
        } else
        {
            takenBlack.Add(token.transform);
        }
    }

    void Update()
    {
        
    }


    private void Start()
    {
        if (instance != null && instance != this)
        {
            Destroy(instance);
        }
        instance = this;
    }

    private static TakenPiecesManager instance;

    public static TakenPiecesManager GetInstance()
    {
        return instance;
    }
}
