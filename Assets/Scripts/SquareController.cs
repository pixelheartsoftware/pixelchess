﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SquareController : MonoBehaviour
{
    public PieceController piece { get; private set; }

    internal Coordinate coord { get; set; }

    float squareWidth; 
    float squareHeight;

    private BoxCollider2D _collider;

    private SpriteRenderer _spriteRenderer;

    // Start is called before the first frame update
    void Start()
    {
        _collider = GetComponent<BoxCollider2D>();

        _collider.size = new Vector2(squareWidth, squareHeight);

        _spriteRenderer = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    internal void SetSize(float squareWidth, float squareHeight)
    {
        this.squareWidth = squareWidth;
        this.squareHeight = squareHeight;
    }


    internal void PutPiece(PieceController piece)
    {
        this.piece = piece;
    }

    public void Hihglight(bool highlight)
    {
        _spriteRenderer.enabled = highlight;
    }

    internal bool isFree()
    {
        return piece == null;
    }

    internal bool isOccupied(PieceColor pieceColor)
    {
        return piece != null && piece.color == pieceColor;
    }
}
