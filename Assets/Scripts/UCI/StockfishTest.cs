﻿using System;
using System.Collections;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using UnityEngine;
using System.Collections.Concurrent;


public class StockfishTest : MonoBehaviour
{
    System.Diagnostics.Process myProcess;

    private ConcurrentDictionary<ResponseType, string> communication = new ConcurrentDictionary<ResponseType, string>();

    // Start is called before the first frame update
    async void Start()
    {
        try
        {
            string status = await EngineBoot();
        }
        catch (Exception e)
        {
            print(e);
        }
    }

    public async Task NewGame()
    {
        await SendCommand(Message.Uci());
        await SendCommand(Message.SetOption("UCI_LimitStrength", "true"));
        await SendCommand(Message.SetOption("UCI_Elo", "1350"));
        await SendCommand(Message.SetOption("Hash", "1"));
        await SendCommand(Message.SetOption("Skill Level", "0"));
        await SendCommand(Message.UciNewGame());
        await SendCommand(Message.IsReady());
    }

    public async Task Move(string seq)
    {
        await SendCommand(Message.Position(seq));
        await SendCommand(Message.Go());
    }

    public async Task<ComputerResponse> GetComputerResponse()
    {
        string response = await SendCommand(Message.Stop());

        

        return new ComputerResponse(response);
    }

    async Task<string> EngineBoot()
    {
        myProcess = new System.Diagnostics.Process();
        myProcess.StartInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
        myProcess.StartInfo.CreateNoWindow = true;
       
        myProcess.StartInfo.UseShellExecute = false;
        //myProcess.StartInfo.FileName = "C:\\stockfish_20090216_x64_bmi2.exe";
        myProcess.StartInfo.FileName = Application.dataPath +  "\\stockfish_20090216_x64_bmi2.exe";

        myProcess.EnableRaisingEvents = true;
        myProcess.StartInfo.RedirectStandardOutput = true;
        myProcess.StartInfo.RedirectStandardInput = true;
        myProcess.Start();

        // engine message read-out:
        string output;
        while ((output = await myProcess.StandardOutput.ReadLineAsync()) != null)
        {
            Debug.Log(output);
            ResponseType respType = GetResponseType(output);

            if (respType != ResponseType.unknown)
            {
                communication.TryAdd(respType, output);
            }
        }

        return "FINISHED";
    }

    private ResponseType GetResponseType(string output)
    {
        ResponseType[] values = (ResponseType[])Enum.GetValues(typeof(ResponseType));

        foreach (ResponseType type in values)
        {
            if (output.StartsWith(type.ToString()))
            {
                return type;
            }
        }

        return ResponseType.unknown;
    }

    private async Task<string> SendCommand(Message message)
    {
        if (myProcess == null || myProcess.HasExited)
        {
            throw new Exception("Engine not initialized or exited.");
        }

        myProcess.StandardInput.WriteLine(message.ToString());

        if (message.responseType != ResponseType.none)
        {
            return await Task.Run(() => WaitForResponse(message.responseType));
        }
        return null;
    }


    string WaitForResponse(ResponseType responseType)
    {
        string response = null;
        while (response == null)
        {
            if (communication.ContainsKey(responseType))
            {
                communication.TryRemove(responseType, out response);
            }
        }

        return response;
    }
}

public class Message
{
    public RequestType requestType { get; set; }
    public string to { get; set; }

    public ResponseType responseType { get; set; }
    public string from { get; set; }

    private Message(RequestType requestType, string to, ResponseType responseType)
    {
        this.requestType = requestType;
        this.to = to;
        this.responseType = responseType;
    }
    private Message(RequestType requestType, ResponseType responseType)
    {
        this.requestType = requestType;
        this.responseType = responseType;
    }
    public static Message Uci()
    {
        return new Message(RequestType.uci, ResponseType.uciok);
    }
    public static Message UciNewGame()
    {
        return new Message(RequestType.ucinewgame, ResponseType.none);
    }
    public static Message IsReady()
    {
        return new Message(RequestType.isready, ResponseType.readyok);
    }
    public static Message Position(string moveSeq)
    {
        return new Message(RequestType.position, "startpos moves " + moveSeq, ResponseType.none);
    }
    public static Message Go()
    {
        return new Message(RequestType.go, ResponseType.info);
    }
    public static Message Stop()
    {
        return new Message(RequestType.stop, ResponseType.bestmove);
    }
    public static Message SetOption(string option, string value)
    {
        return new Message(RequestType.setoption, " name " + option + " value " + value, ResponseType.none);
    }

    public override string ToString()
    {
        return requestType.ToString() + " " + to;
    }
}

public enum RequestType
{
    uci, ucinewgame, isready, position, go, stop, setoption
}

public enum ResponseType
{
    none, uciok, readyok, info, bestmove, unknown
}