using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Text.RegularExpressions;
using System;

public class ComputerResponse
{
    public Move bestMove;
    
    public Move ponder;

    public ComputerResponse(string responseString)
    {
        Match m = Regex.Match(responseString, @"bestmove (\w*){1}( ponder \w*)?");

        bestMove = ParseMove(m.Groups[1].Value);
        ponder = ParseMove(m.Groups[2].Value);
    }

    private Move ParseMove(string moveString)
    {
        Match m = Regex.Match(moveString, @"([a-h]{1}[0-9]{1})([a-h]{1}[0-9]{1}).*");

        Coordinate from = Coordinate.fromUci(m.Groups[1].Value);
        Coordinate to = Coordinate.fromUci(m.Groups[2].Value);

        return new Move(from, to);
    }
}
