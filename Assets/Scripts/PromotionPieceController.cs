using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PromotionPieceController : MonoBehaviour
{
    public PieceType type;
    public void Selected()
    {
        ChessboardManager.GetInstance().Promote(type);
    }
}
