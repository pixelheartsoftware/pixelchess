﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PieceController : Enchantable<PieceController>
{
    public Piece piece;

    public bool dragged = false;

    internal Coordinate coord { 
        get 
        {
            return square.coord;
        } 
    }

    internal PieceType type
    {
        get
        {
            return piece.type;
        }
    }
    internal PieceColor color
    {
        get
        {
            return piece.color;
        }
    }

    public SquareController square { get; set; }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (dragged)
        {
            return;
        }
        if (transform.position != square.transform.position)
        {
            if (Vector2.Distance(transform.position, square.transform.position) > 0.1F)
            {
                transform.position = Vector2.Lerp(transform.position, square.transform.position, 0.1F);
            } else
            {
                transform.position = square.transform.position;
            }
        }
    }

    internal void PutOnSquare(SquareController square)
    {
        this.square = square;
    }

}
