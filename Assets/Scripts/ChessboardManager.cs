﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using UnityEngine;
using static Coordinate;
using static PieceType;

public class ChessboardManager : MonoBehaviour
{
    public float borderLeft = 0;
    public float borderRight = 0;
    public float borderTop = 0;
    public float borderBottom = 0;

    public GameObject squarePrefab;

    private Dictionary<Coordinate, SquareController> squares = new Dictionary<Coordinate, SquareController>();

    private GameDescription gameDescription = new GameDescription();

    private SpriteRenderer _spriteRenderer;

    public StockfishTest stockfish;

    public PieceColor sideToMove = PieceColor.White;
    public PieceColor playerSide = PieceColor.White;
    private ComputerResponse computerMove;
    private Move ponder;
    private bool computerClockCommitted = false;
    public Move playerMove { get; set; }
    private bool playerClockCommitted = false;
    private bool playerMoved = false;

    public GameObject promotionPanel;

    void Update()
    {
        if (sideToMove == playerSide)
        {
            if (playerMove == null)
            {
                // The player has not yet moved
                return;
            }

            if (!playerClockCommitted)
            {
                if (!playerMoved)
                {
                    if (MovesUtil.IsPromotion(playerMove, gameDescription) && playerMove.promotion == PieceType.Pawn)
                    {
                        promotionPanel.SetActive(true);
                        return;
                    }

                    bool canMove = Move(playerMove);

                    if (!canMove)
                    {
                        playerMove = null;
                        playerMoved = false;
                        return;
                    }

                    playerMoved = true;
                }
            }
            else
            {
                sideToMove = sideToMove.Opposite();
                playerMove = null;
                playerMoved = false;
                Task.Run(WaitForMove);
            }

        } else
        {
            ComputerResponse compMove = Interlocked.CompareExchange<ComputerResponse>(ref computerMove, null, null);
            if (compMove == null)
            {
                // The computer has not yet moved
                return;
            }

            ponder = compMove.ponder;

            ChessClockController.GetInstance().SwitchLeft();
            Move(compMove.bestMove);
            sideToMove = sideToMove.Opposite();
            Interlocked.Exchange(ref computerMove, null);
        }
    }

    public bool CommitClock(bool player)
    {
        if (player)
        {
            if (playerMove == null)
            {
                return false;
            }
            playerClockCommitted = true;
            computerClockCommitted = false;
        } else
        {
            if (computerMove == null)
            {
                return false;
            }
            playerClockCommitted = false;
            computerClockCommitted = true;
        }
        return true;
    }

    /**
     * Moves given piece to the given square if possible, returns false othervise. 
     */
    public bool Move(Move move)
    {
        PieceController piece = squares[move.from].piece;
        SquareController toSquare = squares[move.to];

        if (piece.color != sideToMove)
        {
            return false;
        }

        HashSet<Move> moves = MovesUtil.GetMoves(piece.coord, gameDescription);
        if (!moves.Contains(move))
        {
            return false;
        }

        piece.square.PutPiece(null);

        if (toSquare.piece != null)
        {
            if (toSquare.isOccupied(piece.color.Opposite()))
            {
                Take(toSquare);
            }
        } else if (MovesUtil.IsEnPassant(move, gameDescription))
        {
            Coordinate take = MovesUtil.GetEnPassantTakeCoord(move);
            Take(squares[take]);
        }

        if (move.promotion != PieceType.Pawn)
        {
            PieceController newPiece = PiecesManager.GetInstance().CreatePiece(move.promotion, piece.color);

            toSquare.PutPiece(newPiece);
            newPiece.PutOnSquare(toSquare);

            Destroy(piece.gameObject);
            piece = newPiece;
        } else
        {
            toSquare.PutPiece(piece);
            piece.PutOnSquare(toSquare);
        }

        if (move.IsCastling() == CastlingType.OO)
        {
            if (piece.color == PieceColor.White)
            {
                PieceController rook = squares[Coordinate.H1].piece;
                squares[Coordinate.F1].PutPiece(rook);
                squares[Coordinate.H1].PutPiece(null);
                rook.PutOnSquare(squares[Coordinate.F1]);
            } else
            {
                PieceController rook = squares[Coordinate.H8].piece;
                squares[Coordinate.F8].PutPiece(rook);
                squares[Coordinate.H8].PutPiece(null);
                rook.PutOnSquare(squares[Coordinate.F8]);
            }
        } else if (move.IsCastling() == CastlingType.OOO)
        {
            if (piece.color == PieceColor.White)
            {
                PieceController rook = squares[Coordinate.A1].piece;
                squares[Coordinate.D1].PutPiece(rook);
                squares[Coordinate.A1].PutPiece(null);
                rook.PutOnSquare(squares[Coordinate.D1]);
            }
            else
            {
                PieceController rook = squares[Coordinate.A8].piece;
                squares[Coordinate.D8].PutPiece(rook);
                squares[Coordinate.A8].PutPiece(null);
                rook.PutOnSquare(squares[Coordinate.D8]);
            }
        }


        gameDescription.Move(move);

        return true;
    }

    private async Task WaitForMove()
    {
        await stockfish.Move(gameDescription.UciMoveSequence());

        Thread.Sleep(100);

        ComputerResponse compMove = await stockfish.GetComputerResponse();

        Interlocked.Exchange(ref computerMove, compMove);
    }


    private void Take(SquareController square)
    {
        TakenPiecesManager.GetInstance().Take(square.piece);

        Destroy(square.piece.gameObject);

        square.PutPiece(null);
    }

    public void Put(PieceController piece, SquareController square)
    {
        if (piece.square != null)
        {
            piece.square.PutPiece(null);
        }

        square.PutPiece(piece);

        piece.PutOnSquare(square);

        gameDescription.Put(square.coord, piece.piece);
    }
    internal Move GetPonder()
    {
        return ponder;
    }

    internal void HighlightMove(Move move)
    {
        squares[move.from].Hihglight(true);
        squares[move.to].Hihglight(true);
    }

    internal void HighlightLegalMoves(PieceController piece)
    {
        if (piece.color != sideToMove)
        {
            return;
        }

        HashSet<Move> moves = MovesUtil.GetMoves(piece.coord, gameDescription);

        foreach (Move move in moves)
        {
            squares[move.to].Hihglight(true);
        }
    }

    internal void UnhighlightLegalMoves()
    {
        foreach (SquareController sq in squares.Values)
        {
            sq.Hihglight(false);
        }
    }

    internal void Promote(PieceType type)
    {
        promotionPanel.SetActive(false);
        playerMove = new Move(playerMove.from, playerMove.to, type);
    }


    void Start()
    {
        if (instance != null && instance != this)
        {
            GameObject.Destroy(instance);
        }
        instance = this;

        _spriteRenderer = gameObject.GetComponent<SpriteRenderer>();

        float squareWidth = (_spriteRenderer.sprite.bounds.size.x - (borderLeft + borderRight)) / 8;
        float squareHeight = (_spriteRenderer.sprite.bounds.size.y - (borderTop + borderBottom)) / 8;

        float currentPosX = (gameObject.transform.position.x - _spriteRenderer.sprite.bounds.extents.x) + borderLeft + (squareWidth/2);

        for (int i = 0; i < 8; i++)
        {
            float currentPosY = (gameObject.transform.position.y - _spriteRenderer.sprite.bounds.extents.y) + borderBottom + (squareHeight / 2);
            for (int j = 0; j < 8; j++)
            {
                Coordinate coord = Coordinate.Get((Coordinate.File)i, (Coordinate.Rank)j);

                SquareController sq = CreateSquare(coord, currentPosX, currentPosY, squareWidth, squareHeight);

                squares.Add(coord, sq);

                currentPosY += squareHeight;
            }
            currentPosX += squareWidth;
        }

        Reset();

        stockfish.NewGame();
    }

    private SquareController CreateSquare(Coordinate coord, float currentPosX, float currentPosY, float squareWidth, float squareHeight)
    {
        GameObject go = Instantiate(squarePrefab, new Vector2(currentPosX, currentPosY), gameObject.transform.rotation, transform);
        go.name = coord.ToString();

        SquareController sq = go.GetComponent<SquareController>();

        sq.SetSize(squareWidth, squareHeight);
        sq.coord = coord;

        return sq;
    }

    public void Reset()
    {
        // whites
        Put(PiecesManager.GetInstance().CreatePiece(Rook, PieceColor.White),   squares[A1]);
        Put(PiecesManager.GetInstance().CreatePiece(Knight, PieceColor.White), squares[B1]);
        Put(PiecesManager.GetInstance().CreatePiece(Bishop, PieceColor.White), squares[C1]);
        // magic one
        PieceController magicPiece = PiecesManager.GetInstance().CreatePiece(Queen, PieceColor.White);
        EnchantmentController.GetInstance().NewBestMoveEnchantment().Enchant(magicPiece);
        Put(magicPiece,  squares[D1]);
        Put(PiecesManager.GetInstance().CreatePiece(King, PieceColor.White), squares[E1]);
        Put(PiecesManager.GetInstance().CreatePiece(Bishop, PieceColor.White), squares[F1]);
        Put(PiecesManager.GetInstance().CreatePiece(Knight, PieceColor.White), squares[G1]);
        Put(PiecesManager.GetInstance().CreatePiece(Rook, PieceColor.White),   squares[H1]);

        Put(PiecesManager.GetInstance().CreatePiece(Pawn, PieceColor.White),   squares[A2]);
        Put(PiecesManager.GetInstance().CreatePiece(Pawn, PieceColor.White),   squares[B2]);
        Put(PiecesManager.GetInstance().CreatePiece(Pawn, PieceColor.White),   squares[C2]);

        // magic one
        magicPiece = PiecesManager.GetInstance().CreatePiece(Pawn, PieceColor.White);
        EnchantmentController.GetInstance().NewBestMoveEnchantment().Enchant(magicPiece);
        Put(magicPiece,   squares[D2]);

        Put(PiecesManager.GetInstance().CreatePiece(Pawn, PieceColor.White),   squares[E2]);
        Put(PiecesManager.GetInstance().CreatePiece(Pawn, PieceColor.White),   squares[F2]);
        Put(PiecesManager.GetInstance().CreatePiece(Pawn, PieceColor.White),   squares[G2]);
        Put(PiecesManager.GetInstance().CreatePiece(Pawn, PieceColor.White),   squares[H2]);

        
        Put(PiecesManager.GetInstance().CreatePiece(Rook, PieceColor.Black),   squares[A8]);
        Put(PiecesManager.GetInstance().CreatePiece(Knight, PieceColor.Black), squares[B8]);
        Put(PiecesManager.GetInstance().CreatePiece(Bishop, PieceColor.Black), squares[C8]);
        Put(PiecesManager.GetInstance().CreatePiece(Queen, PieceColor.Black),  squares[D8]);
        Put(PiecesManager.GetInstance().CreatePiece(King, PieceColor.Black), squares[E8]);
        Put(PiecesManager.GetInstance().CreatePiece(Bishop, PieceColor.Black), squares[F8]);
        Put(PiecesManager.GetInstance().CreatePiece(Knight, PieceColor.Black), squares[G8]);
        Put(PiecesManager.GetInstance().CreatePiece(Rook, PieceColor.Black),   squares[H8]);

        Put(PiecesManager.GetInstance().CreatePiece(Pawn, PieceColor.Black),   squares[A7]);
        Put(PiecesManager.GetInstance().CreatePiece(Pawn, PieceColor.Black),   squares[B7]);
        Put(PiecesManager.GetInstance().CreatePiece(Pawn, PieceColor.Black),   squares[C7]);
        Put(PiecesManager.GetInstance().CreatePiece(Pawn, PieceColor.Black),   squares[D7]);
        Put(PiecesManager.GetInstance().CreatePiece(Pawn, PieceColor.Black),   squares[E7]);
        Put(PiecesManager.GetInstance().CreatePiece(Pawn, PieceColor.Black),   squares[F7]);
        Put(PiecesManager.GetInstance().CreatePiece(Pawn, PieceColor.Black),   squares[G7]);
        Put(PiecesManager.GetInstance().CreatePiece(Pawn, PieceColor.Black),   squares[H7]);
    }

    private static ChessboardManager instance;

    public static ChessboardManager GetInstance()
    {
        return instance;
    }
}