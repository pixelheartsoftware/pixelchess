using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChessClockController : MonoBehaviour
{
    public static float SIX_OCLOCK_SECONDS = 6 * 3600;

    public int initialLeftHours = 0;
    public int initialLeftMinutes = 5;
    private float leftSeconds;

    public int initialRightHours = 0;
    public int initialRightMinutes = 5;
    private float rightSeconds;

    private bool leftActive = false;
    private bool rightActive = false;
    private float lastTime;

    public Transform leftBigHand;
    public Transform rightBigHand;

    public Transform leftSmallHand;
    public Transform rightSmallHand;

    public Transform leftButton;
    public Transform rightButton;

    private float buttonCenterY;
    public float buttonPressDistance = 0.1f;

    public float timeScale = 1;

    private float addedTime = 0;


    private void Start()
    {
        buttonCenterY = leftButton.transform.position.y;
        rightButton.transform.position = new Vector2(rightButton.transform.position.x, buttonCenterY);

        leftSeconds = ToSeconds(initialLeftHours, initialLeftMinutes);
        SetLeftHands();

        rightSeconds = ToSeconds(initialRightHours, initialRightMinutes);
        SetRightHands();

        // Singleton
        if (instance != null && instance != this)
        {
            Destroy(instance);
        }
        instance = this;
    }

    private static ChessClockController instance;

    public static ChessClockController GetInstance()
    {
        return instance;
    }

    public void SwitchLeft()
    {
        bool canCommit = ChessboardManager.GetInstance().CommitClock(false);

        if (!canCommit)
        {
            return;
        }

        if (leftActive || (rightActive == leftActive))
        {
            Switch();
        }
    }

    public void SwitchRight()
    {
        bool canCommit = ChessboardManager.GetInstance().CommitClock(true);

        if (!canCommit)
        {
            return;
        }

        if (rightActive || (rightActive == leftActive))
        {
            Switch();
        }
    }

    public void Switch()
    {

        if (leftActive == rightActive)
        {
            leftActive = true;
            StartClock();

            leftButton.transform.position = new Vector2(leftButton.transform.position.x, buttonCenterY + ((leftActive ? 1 : -1) * buttonPressDistance));
            rightButton.transform.position = new Vector2(rightButton.transform.position.x, buttonCenterY + ((rightActive ? 1 : -1) * buttonPressDistance));
            return;
        }

        CountTime();

        leftActive = !leftActive;
        rightActive = !rightActive;

        leftButton.transform.position = new Vector2(leftButton.transform.position.x, buttonCenterY + ((leftActive ? 1 : -1) * buttonPressDistance));
        rightButton.transform.position = new Vector2(rightButton.transform.position.x, buttonCenterY + ((rightActive ? 1 : -1) * buttonPressDistance));
    }

    public void StartClock()
    {
        lastTime = Time.realtimeSinceStartup;
    }

    private void SetSmallHand(Transform hand, float seconds)
    {
        seconds = SIX_OCLOCK_SECONDS - seconds;

        float hours = (seconds) / 3600;

        float angle = 360 - 360 * (hours / 12);

        hand.rotation = Quaternion.Euler(0, 0, angle);
    }

    private void SetBigHand(Transform hand, float seconds)
    {
        seconds = SIX_OCLOCK_SECONDS - seconds;

        float minutes = seconds / 60;
        float minutesRest = minutes % 60;

        float angle = 360 - 360 * (minutesRest / 60);

        hand.rotation = Quaternion.Euler(0, 0, angle);
    }

    private float ToSeconds(int hours, int minutes)
    {
        return hours * 3600 + minutes * 60;
    }

    // Update is called once per frame
    void Update()
    {
        CountTime();
    }

    private void CountTime()
    {
        float timeSinceLast = Time.realtimeSinceStartup - lastTime;
        lastTime = Time.realtimeSinceStartup;

        timeSinceLast *= timeScale;

        if (leftActive)
        {
            leftSeconds -= timeSinceLast;
            SetLeftHands();
        }
        if (rightActive)
        {
            rightSeconds -= timeSinceLast;
            SetRightHands();
        }

        if (addedTime > 0)
        {
            float toAdd = 0.1F * addedTime;

            rightSeconds += toAdd;
            addedTime -= toAdd;

            if (addedTime < 0.001)
            {
                addedTime = 0;
            }
            SetRightHands();
        }
    }

    public void IncreasePlayerTime(float timeInMinutes)
    {
        addedTime += timeInMinutes * 60;
    }

    private void SetLeftHands()
    {
        SetBigHand(leftBigHand, leftSeconds);
        SetSmallHand(leftSmallHand, leftSeconds);
    }    
    
    private void SetRightHands()
    {
        SetBigHand(rightBigHand, rightSeconds);
        SetSmallHand(rightSmallHand, rightSeconds);
    }
}
