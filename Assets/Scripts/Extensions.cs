﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Extensions
{
    public static PieceColor Opposite(this PieceColor color)
    {
        switch (color)
        {
            case PieceColor.Black:
                return PieceColor.White;
            case PieceColor.White:
                return PieceColor.Black;
            default:
                throw new System.Exception("Unknown color: " + color);
        }
    }

    public static string ToUci(this PieceType type)
    {
        switch (type)
        {
            case PieceType.Queen:
                return "q";
            case PieceType.Rook:
                return "r";
            case PieceType.Knight:
                return "k";
            case PieceType.Bishop:
                return "b";
            default:
                return "";
        }
    }
}
